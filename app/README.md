# API em python para envio de menssagens pelo telegram

## API Python utilizando flask.
- Api desenvolvida em flask para envio de mensagens para o Telegram com banco de dados NoSQL MongoDB para armazenar mensagens enviadas.
### Instalação:
* Instalar bibliotecas do projeto
```sh
pip install -r requirements.txt
```
* Obter o token do bot criado no telegram e realizar o export da variável .
```sh
export TOKEN="28736473243546234"
```
* exportar URI do MongoDB
```sh
export MONGO_URI="127.0.0.1"
```
* Executar a API
```sh
python3 app.py
```
### Modo de utilização
* EndPoints:
1. /healthcheck : Utilizado para checar disponibilidade da api utilizando método get retorna mensagem "API is alive".
```sh
curl 34.229.133.211/healthcheck 
```
2. /enviar : Utilizado para enviar mesagens para um usuário específico do telegram.
```sh
curl -s -X POST http://34.229.133.211/enviar/478335909 -H 'content-type: application/json' -d '{ "msg":"Boa Tarde!" }'
```
3. /listar :  Utilizado para listar todas mensagens enviadas.
```sh
curl 34.229.133.211/listar
```
4. /metrics : Utilizado para enviar as métricas da API para o prometheus.

### Deploy Cluster K8S
1. api-deployment.yaml, contém o manifesto para deploy da API.
2. mongo-deployment.yaml, é o manifesto para deploy do MongoDB.
3. dashboard-api-grafana.yaml, contém ConfigMap com JSON do dashboard da API no grafana.
4. grafana-svc.yaml, exporta os serviços do grafana tipo nodePort para acesso HA Proxy.