import os
import requests
import time
import random
from flask import Flask, Response, request
from flask_restful import Api
from flask_restful import Resource
from flask_restful import reqparse
from flask_pymongo import PyMongo
from bson import json_util
from prometheus_client import Counter, generate_latest
import logging

token = os.getenv('TOKEN') # Importa o token da vairável de ambient
logger = logging.getLogger(__name__)

app = Flask(__name__)
app.config.from_object('settings.ProdConfig')
mongo = PyMongo(app)
api = Api(app)

# Recebe Contadores Total de Requisições
api_request_total = Counter('api_request_total', 'Numero total de requisições', ['method', 'endpoint'])

# Classe para fornecer as metricas
class Metrics(Resource):
    def get(self):
        return Response(generate_latest(), mimetype="text/plain")

# Classe para checar se API está Health
class HealthCheck(Resource):
    def get(self):
        api_request_total.labels('get', '/healthcheck').inc()
        return {"message": "API is alive"}

# Classe que recebe o id do usuário do telegram e a mensagem que vai enviar
class MessageTelegram(Resource):
    parse = reqparse.RequestParser()
    parse.add_argument('msg', type=str, required=True, help='Você deve inserir a messagem para o enviar')
    def post(self, id):
        data = MessageTelegram.parse.parse_args()
        msg = data['msg']
        send_msg = SendMessage.telegram_message(token, id, msg) # Chama a Classe que envia msg para o telegram passando o id e msg
        message_id = (send_msg['result']['message_id'])
        first_name = (send_msg['result']['chat']['first_name'])
        last_name = last_name = (send_msg['result']['chat']['last_name'])
        text = (send_msg['result']['text'])
        save_msg(message_id, first_name, last_name, text)
        api_request_total.labels('post', '/enviar').inc()
        return {"Mensagem": "Enviada" }

# Classe que lista mensagens enviadas
class ListMensagens(Resource):
    def get(self):
        mensagens = mongo.db.message.find()
        response = json_util.dumps(mensagens)
        api_request_total.labels('get', '/listar').inc()
        return Response(response, mimetype='application/json')

# Classe que envia a mensagem utilizando api do telegram
class SendMessage(object):
    @classmethod
    def telegram_message(cls, token, bot_chat_id, bot_message):
        send = 'https://api.telegram.org/bot' + token + '/sendMessage?chat_id=' + bot_chat_id + '&parse_mode=Markdown&text=' + bot_message
        response = requests.get(send)
        return response.json()

def save_msg(message_id, first_name, last_name, text):
    mongo.db.message.insert(
        {'mensagens': {'message_id': message_id, 'first_name': first_name, 'last_name': last_name, 'text': text }}
    )

# Instânciando nossas rotas "endpoint"
api.add_resource(HealthCheck, '/healthcheck') 
api.add_resource(MessageTelegram, '/enviar/<string:id>')
api.add_resource(ListMensagens, '/listar')
api.add_resource(Metrics, '/metrics')

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5000, threaded=True)