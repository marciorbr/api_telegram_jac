# Configuração do serviços com Ansible
## Código Ansible para configuração das instâncias EC2
### Serviços que são configurandos:
1. Instalação do K3S - [master,worker]
2. Instalação do Helm - [master]
3. Faz o deploy da Stack Prometheus com Helm
4. Realiza o Join dos nodes workers
5. Instala e realizar o registro do gitlab-runner
6. Instala e configura o HA Proxy

## Procedimento
1. Crie o arquivo vault.yml em vars e add o token do runner obtido do gitlab
```sh
ansible-vault create vars/vault.yml
```
2. Execute o playbook
```sh
ansible-playbook playbook.yaml --ask-vault-pass
```