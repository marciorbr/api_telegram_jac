provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_key_pair" "aws_k3s" {
  key_name   = "k3s_key_pair"
  public_key = file("id_rsa.pub")
}

resource "aws_security_group" "aws_k3s" {
  name        = "k3s-security-group"
  description = "Allow SSH traffic"

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "All_Trafic_nodes"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["172.31.0.0/16"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "k3s-security-group",
    Projeto = "JAC"
  }
}

resource "aws_security_group" "aws_haproxy" {
  name        = "haproxy-security-group"
  description = "Allow HTTP, HTTPS"

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "haproxy-security-group",
    Projeto = "JAC"
  }
}

resource "aws_instance" "master" {
  count = 1
  key_name      = aws_key_pair.aws_k3s.key_name
  ami           = "ami-042e8287309f5df03"
  instance_type = "t2.medium"

  tags = {
    Name = "k3s-master-${count.index}",
    Projeto = "JAC"
  }

  vpc_security_group_ids = [
    aws_security_group.aws_k3s.id
  ]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("key")
    host        = self.public_ip
  }

  ebs_block_device {
    device_name = "/dev/sda1"
    volume_type = "gp2"
    volume_size = 30
  }
  
}

resource "aws_instance" "worker" {
  count = 1
  key_name      = aws_key_pair.aws_k3s.key_name
  ami           = "ami-03d315ad33b9d49c4"
  instance_type = "t2.micro"

  tags = {
    Name = "k3s-worker-${count.index}",
    Projeto = "JAC"
  }

  vpc_security_group_ids = [
    aws_security_group.aws_k3s.id
  ]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("key")
    host        = self.public_ip
  }

  ebs_block_device {
    device_name = "/dev/sda1"
    volume_type = "gp2"
    volume_size = 30
  }
  
}

resource "aws_instance" "runner" {
  count = 1
  key_name      = aws_key_pair.aws_k3s.key_name
  ami           = "ami-03d315ad33b9d49c4"
  instance_type = "t2.micro"

  tags = {
    Name = "runner-${count.index}",
    Projeto = "JAC"
  }

  vpc_security_group_ids = [
    aws_security_group.aws_k3s.id
  ]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("key")
    host        = self.public_ip
  }

  ebs_block_device {
    device_name = "/dev/sda1"
    volume_type = "gp2"
    volume_size = 30
  }
}

resource "aws_instance" "haproxy" {
  count = 1
  key_name      = aws_key_pair.aws_k3s.key_name
  ami           = "ami-03d315ad33b9d49c4"
  instance_type = "t2.micro"

  tags = {
    Name = "haproxy",
    Projeto = "JAC"
  }

  vpc_security_group_ids = [
    aws_security_group.aws_k3s.id,
    aws_security_group.aws_haproxy.id
  ]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("key")
    host        = self.public_ip
  }

  ebs_block_device {
    device_name = "/dev/sda1"
    volume_type = "gp2"
    volume_size = 30
  } 
}