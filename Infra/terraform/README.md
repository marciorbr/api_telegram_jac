# Provisionando instâncias Ec2 AWS com Terraform
## Código terraform para provisionamento de instâncias EC2
### Recursos que o código criar na AWS
1. "key_pair" Chave para acesso ssh as VM.
2. "security_group" Um para acesso externo ssh '22', outro para acesso externo http '80' e http '3000'.
3. "aws_instance" São criadas 4 tipos de instance: 1 master, 2 worker, 1 haproxy e 1 runner.

## Procedimento
1. Adicione a key id_rsa.pub na pasta do Infra do projeto.
2. Valide o código:
```sh
terraform validade
```
3. Crie as instâncias
```sh
terraform apply
```