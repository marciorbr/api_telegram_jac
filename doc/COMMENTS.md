# AWS
## Problemas:
1. Utilizar master com instance do tipo t2.micro travando a VM pro consumo de CPU na hora de subir stack prometheus.
- Foi utilizado t2.medium

# Provisionamento da Infra com terraform
## Sugestões:
1. Como boa prática para se trabalhar em time de múltiplos usuários os arquivos .tfstate deveriam ser armazenados em bucket AWS S3.
2. Para reaproveitamento de código deveria ser criados módulos de resources evitando assim repetição de código.
## Probelemas:

# Usando Ansible para gerência de configuração
## Problemas:
1. Ter que pegar os Ips das instâncias ec2 para adicinar no inventario do ansible de forma manual
 - Solução: Usar templatefile Function do terraform para pegar o output e gerar o inventory de forma dinâmica
## Sugestões:
1. Dividir as tasks no playbook.yml em roles separando os serviços, k3s, runner, haproxy
2. Add variáveis para melhor flexbilidade de manutenção

# API Python
## Problemas:
1. Pouco conhecimento em python "venho estudando a 4 meses"
## Sugestões:
1. Poderia restringir acesso ao endipoint /metrics para ser acessado apenas dentro do cluster, seria possivel se utilizar ingress traefik 
2. Conexão com o banco MondoDB está sem autenticação

# Kubernetes
## Sugestões:
1. Seria melhor usar o ingress traefik que já vem no k3s, não foi utilizado por não ter uma dns
2. Não há nenhuma persistência nos PODs, poderia utilizar alguma storage